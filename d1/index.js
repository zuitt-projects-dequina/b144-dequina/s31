// Modules and Parameter Routes

/*
	Separation of Concerns
		// Modules and Parameter Routes
		// hinahati ung bawat codes, separated files
		// we separate our concerns through the use of JS Modules

		why do we separate concerns?
			- for better code readability
			- Improved scalability
			- better code maintainability
		what do we separate?
			- models
				- contains WHAT objects are needed in our API (eg. Schemas)
			- controllers
				- contains instyructions on HOW your API will perform its inteded tasks
				- mongoose model queries are used here (model.find() etc)
			- routes ()
				- defines WHEN particular controllers will be used
				- eg. "/<endpoint>"

	to understand clearly... lets CODE ALONG
*/	

// Set up the dependencies
const express = require('express')
const mongoose = require('mongoose')
// This allows us to use all the routes defined in 'taskRoute.js'
const taskRoute = require('./routes/taskRoute')

// Server Setup
const app = express()
const port = 3001;
app.use(express.json())
app.use(express.urlencoded({extended : true}))

// Database connection
mongoose.connect("mongodb+srv://ralphdequina:ralphdequina@wdc028-course-booking.mwzyk.mongodb.net/batch144-to-do?retryWrites=true&w=majority", {
	// to avoid/prevents future error in our connection to MongoDB
	useNewUrlParser: true, 
	useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"))
db.once("open", () => console.log("We're connected to the cloud database."))



// Routes (Base URI for task route)
// This allows all the task routes created in the taskRoute.js file to use "/tasks" route
app.use("/tasks", taskRoute)
// http://localhost:3001/tasks



app.listen(port, () => console.log(`Now listening to port ${port}`));
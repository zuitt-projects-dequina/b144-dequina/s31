const express = require('express');
const taskController = require('../controllers/taskController')

// Create a router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router()


// Route to get all the tasks
router.get("/", (req, res) => {
	taskController.getAllTask().then(resultFromController => res.send(resultFromController))
})


// Route for creating a task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(result => res.send(result));
})


// Route for deleting a task
// http://localhost:3001/tasks/:id   <- this is what we want
// the "colon" (:) is an indetifier that helphs create a dynamic route which allows us to supply information in the url
// The word that comes after the colon symbol will be the name of the URL parameter
// ":id" is called WILDCARD where you can put any value, it then creates a link between "id" parameter in the URL and the value provided in the URL
// ":id" having colon is an identifier, that allows us to supply a URL 
router.delete("/:id", (req, res) => {
	// the URL parameter values are accessed via the request object's "paramas" property
	// the property name  of this object will match the given URL parameter name
	// In this case "id" is the name of the parameter
	taskController.deleteTask(req.params.id).then(result => res.send(result))
});


// Route for updating a Task
router.put("/:id", (req, res)=> {
	taskController.updateTask(req.params.id, req.body).then(result => res.send(result))
})


module.exports = router;



//////// S31 Activity ////////

// Get Soecific Task
router.get("/:id", (req, res) => {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


// Update Specific Task
router.put("/:id/complete", (req, res)=> {
	taskController.updateTaskStatus(req.params.id, req.body).then(result => res.send(result))
})